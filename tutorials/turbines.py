import matplotlib.pyplot as plt
import xarray as xr
import numpy as np
import s3fs
import cartopy.crs as ccrs
import pyproj

ds = xr.open_zarr(
    s3fs.S3Map(
        root="s3://whiffle-wins50-data/data/whiffle_wins50_2050_scenario_turbines.zarr",
        s3=s3fs.S3FileSystem(anon=True),
    )
)

ds.info()

# list and plot windfarms
fig = plt.figure()
fig.set_size_inches(10, 10)
ax = plt.axes(projection=ccrs.UTM(zone="31"))
gl = ax.gridlines(draw_labels=True, x_inline=False, y_inline=False)
ax.coastlines(resolution="10m")

proj = pyproj.CRS.from_user_input(ds.attrs["projection"])
transformer = pyproj.Transformer.from_proj(proj, "EPSG:4326", always_xy=True)

ds_locs = ds[["x", "y", "windfarm_identifier"]].load()

print("windfarms in dataset:")
for windfarm in np.unique(ds_locs["windfarm_identifier"]):
    if "landturbines" in windfarm:
        continue

    ds_ = ds_locs.isel(index=np.where(ds_locs["windfarm_identifier"] == windfarm)[0])
    center_coordinate_utm = ds_["x"].mean().values.item(), ds_["y"].mean().values.item()

    center_coordinate_lonlat = transformer.transform(*center_coordinate_utm)

    print(f"{windfarm} (center_coordinate = {center_coordinate_lonlat})")

    ax.plot(ds_["x"], ds_["y"], "o", color="orange", mew=0, ms=1)
    ax.text(
        center_coordinate_utm[0],
        center_coordinate_utm[1],
        windfarm,
        fontsize=4,
        horizontalalignment="center",
        verticalalignment="center",
    )

# plot time series for particular wind farm
ds = ds.sel(time=slice("2020/06/20", "2020/06/27"))
fig, ax = plt.subplots()
select_farm = np.where(ds["windfarm_identifier"] == "future_207")[0]
ds["power"][:, select_farm].sum(axis=1).plot()

plt.show()
